package es.upm.dit.apsv.cris.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Researcher implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String ResearcherId;
	private String name;
	private String password;
	private String lastName;
	private String scopusURL;
	private String Email;
	
	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public Researcher() {
		super();
	}

	public String getResearcherId() {
		return ResearcherId;
	}

	public void setResearcherId(String iD) {
		ResearcherId = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getScopusURL() {
		return scopusURL;
	}

	public void setScopusURL(String scopusURL) {
		this.scopusURL = scopusURL;
	}

	@Override
	public String toString() {
		return "Researcher [ID=" + ResearcherId + ", name=" + name + ", password=" + password + ", lastName=" + lastName
				+ ", scopusURL=" + scopusURL + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ResearcherId == null) ? 0 : ResearcherId.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((scopusURL == null) ? 0 : scopusURL.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Researcher other = (Researcher) obj;
		if (ResearcherId == null) {
			if (other.ResearcherId != null)
				return false;
		} else if (!ResearcherId.equals(other.ResearcherId))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (scopusURL == null) {
			if (other.scopusURL != null)
				return false;
		} else if (!scopusURL.equals(other.scopusURL))
			return false;
		return true;
	}
	
	
	
}
